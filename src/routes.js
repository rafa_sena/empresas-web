import React from "react";

import Login from "./pages/Login";
import EnterpriseList from "./pages/EnterpriseList";
import EnterpriseDetails from "./pages/EnterpriseDetails";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import { isUserAuthenticated } from "./services/Auth";

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/login">
          <Login />
        </Route>
        <PrivateRoute path="/enterprises">
          <EnterpriseList />
        </PrivateRoute>
        <PrivateRoute path="/enterprise/:id">
          <EnterpriseDetails />
        </PrivateRoute>
        <Redirect to="/enterprises" />
      </Switch>
    </Router>
  );
};

const PrivateRoute = ({ children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isUserAuthenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

export default Routes;
