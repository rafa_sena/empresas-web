import axios from "axios";
import { BASE_URL } from "../constants/url";

const http = axios.create({ baseURL: BASE_URL });

export const setHeaders = (headers) => {
  Object.keys(headers).forEach(
    (key) => (http.defaults.headers.common[key] = headers[key])
  );
};

export const baseGetRequest = ({ endpoint, config = {} }) => {
  return http.get(endpoint, config);
};

export const basePostRequest = ({ endpoint, body, config = {} }) => {
  return http.post(endpoint, body, config);
};
