import { basePostRequest, setHeaders } from "./HttpClient";

export let isUserAuthenticated = false;

const setOAuthHeaders = (accessToken, client, uid) => {
  setHeaders({ "access-token": accessToken, uid, client });
};

export const getCredentials = () => {
  const credentials = JSON.parse(localStorage.getItem("credentials"));

  if (credentials) {
    isUserAuthenticated = true;
    setOAuthHeaders(
      credentials.accessToken,
      credentials.client,
      credentials.uid
    );
  }
};

const saveCredentials = (accessToken, client, uid) => {
  const credentials = { accessToken, client, uid };
  localStorage.setItem("credentials", JSON.stringify(credentials));
};

export const signIn = async (email, password) => {
  try {
    const response = await basePostRequest({
      endpoint: "/api/v1/users/auth/sign_in",
      body: { email, password },
    });

    isUserAuthenticated = true;

    setOAuthHeaders(
      response.headers["access-token"],
      response.headers["client"],
      response.headers["uid"]
    );

    saveCredentials(
      response.headers["access-token"],
      response.headers["client"],
      response.headers["uid"]
    );

    return response.data;
  } catch {
    throw new Error("Credenciais informadas são inválidas, tente novamente.");
  }
};
