import { baseGetRequest } from "./HttpClient";

export const getEnterprises = async (name = "") => {
  const response = await baseGetRequest({
    endpoint: "/api/v1/enterprises",
    config: { params: { name } },
  });

  return response.data;
};

export const getEnterprise = async (id) => {
  const response = await baseGetRequest({
    endpoint: `/api/v1/enterprises/${id}`,
  });

  return response.data;
};
