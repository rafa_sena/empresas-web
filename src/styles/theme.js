export const theme = {
  colors: {
    primary: "#383743",
    secondary: "#ee4c77",
    background: "#eeecdb",
    button: "#57bbbc",
    error: "#ff0f44",
    navbarBlue: "#0d0430",
    enterpriseTitle: "#1a0e49",
    enterpriseText: "#8d8c8c",
    searchPlaceholder: "#991237",
    noResultsText: "#b5b4b4",
  },
};
