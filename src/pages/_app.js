import React, { memo } from "react";
import { ThemeProvider } from "styled-components";
import { theme } from "../styles/theme";
import { GlobalStyle } from "../styles/GlobalStyle";
import Routes from "../routes";
import { getCredentials } from "../services/Auth";

const App = memo(() => {
  getCredentials();

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Routes />
    </ThemeProvider>
  );
});

export default App;
