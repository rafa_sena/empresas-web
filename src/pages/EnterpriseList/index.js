/* eslint-disable no-undef */
import EnterpriseListItem from "../../components/EnterpriseListItem";
import Navbar from "../../components/Navbar";
import {
  EnterpriseListContainer,
  EnterpriseListContent,
  NavbarLogo,
  Container,
  EnterpriseWelcomeText,
  Centered,
  EnterpriseEmptyList,
} from "./styles";
import { AiOutlineSearch, AiOutlineClose } from "react-icons/ai";
import { useCallback, useEffect, useState } from "react";
import Input from "../../components/Input";
import { getEnterprises } from "../../services/Enterprises";

import { BASE_URL } from "../../constants/url";
import { useHistory } from "react-router";
import { useDebounce } from "../../hooks/useDebounce";
import LoadingBox from "../../components/LoadingBox";

const EnterpriseList = () => {
  const [enterprises, setEnterprises] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [nameFilter, setNameFilter] = useState("");
  const debouncedNameFilter = useDebounce(nameFilter, 500);

  const [isSearching, setIsSearching] = useState(false);

  const history = useHistory();

  const loadEnterprises = useCallback(async () => {
    if (debouncedNameFilter) {
      setIsLoading(true);
      const response = await getEnterprises(debouncedNameFilter);
      setEnterprises(response.enterprises);
      setIsLoading(false);
    } else {
      setEnterprises([]);
    }
  }, [debouncedNameFilter]);

  const handleEnterpriseClick = useCallback(
    (id) => {
      history.push(`/enterprise/${id}`);
    },
    [history]
  );

  const handleCloseClick = useCallback(() => {
    setIsSearching(false);
    setNameFilter("");
  }, []);

  const noResults =
    !isLoading && debouncedNameFilter && enterprises.length === 0;

  useEffect(loadEnterprises, [loadEnterprises]);

  const navbarContent = isSearching ? (
    <Container>
      <Input
        placeholder="Pesquisar"
        borderColor="white"
        fontColor="white"
        caretColor="white"
        value={nameFilter}
        onChange={(e) => setNameFilter(e.target.value)}
        addonLeft={<AiOutlineSearch size="1.5rem" />}
        addonRight={
          <AiOutlineClose
            size="1.5em"
            cursor="pointer"
            onClick={handleCloseClick}
          />
        }
      />
    </Container>
  ) : (
    <>
      <span />
      <NavbarLogo
        src={`${process.env.PUBLIC_URL}/logo-nav.png`}
        alt="ioasys-logo-white"
      />
      <AiOutlineSearch
        onClick={() => setIsSearching(true)}
        size="2.2em"
        cursor="pointer"
      />
    </>
  );

  return (
    <LoadingBox isLoading={isLoading}>
      <EnterpriseListContainer>
        <Navbar>{navbarContent}</Navbar>
        <EnterpriseListContent>
          {!debouncedNameFilter && (
            <Centered>
              <EnterpriseWelcomeText>
                Clique na busca para iniciar.
              </EnterpriseWelcomeText>
            </Centered>
          )}
          {noResults && (
            <Centered>
              <EnterpriseEmptyList>
                Nenhuma empresa foi encontrada para a busca realizada.
              </EnterpriseEmptyList>
            </Centered>
          )}
          {enterprises.map((enterprise) => (
            <EnterpriseListItem
              key={enterprise.id}
              onClick={() => handleEnterpriseClick(enterprise.id)}
              imageUrl={`${BASE_URL}/${enterprise.photo}`}
              name={enterprise.enterprise_name}
              business={enterprise.enterprise_type.enterprise_type_name}
              location={enterprise.country}
            />
          ))}
        </EnterpriseListContent>
      </EnterpriseListContainer>
    </LoadingBox>
  );
};

export default EnterpriseList;
