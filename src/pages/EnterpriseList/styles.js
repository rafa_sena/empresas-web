import styled from "styled-components";

export const EnterpriseListContainer = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const EnterpriseListContent = styled.div`
  margin-top: 3rem;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  flex: 1;
`;

export const NavbarLogo = styled.img`
  object-fit: contain;
  max-width: 40%;
  max-height: 40%;
`;

export const Container = styled.div`
  display: flex;
  flex: 1;
`;

export const EnterpriseWelcomeText = styled.p`
  color: ${({ theme }) => theme.colors.primary};
  text-align: center;
  align-self: center;
  line-height: 1.22;
  font-size: 2rem;
  letter-spacing: -0.45px;
`;

export const Centered = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const EnterpriseEmptyList = styled.div`
  color: ${({ theme }) => theme.colors.noResultsText};
  font-size: 2.125rem;
  text-align: center;
`;
