import styled from "styled-components";

export const LoginContainer = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const LoginContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 21.938rem;
`;

export const Logo = styled.img`
  width: 18.438rem;
  height: 4.5rem;
  object-fit: contain;
`;

export const WelcomeText = styled.h1`
  margin: 4.063rem 0 1.563rem 0;
  width: 10.75rem;
  font-size: 1.6rem;
  font-weight: bold;
  letter-spacing: -1.2px;
  text-transform: uppercase;
  text-align: center;
  color: ${({ theme }) => theme.colors.primary};
`;

export const WelcomeHelperText = styled.p`
  margin: 1.563rem 0 2.875rem 0;
  font-size: 1.063rem;
  line-height: 1.48;
  letter-spacing: 0.2px;
  text-align: center;
  color: ${({ theme }) => theme.colors.primary};
`;

export const LoginForm = styled.form`
  display: flex;
  flex-direction: column;
  height: 16rem;
  justify-content: space-evenly;
`;

export const LoginButton = styled.button`
  width: 21.75rem;
  height: 3.563rem;
  padding: 1.125rem 8.375rem 1rem 8.438rem;
  border: none;
  border-radius: 3.9px;
  font-size: 1.208rem;
  font-weight: 600;
  text-align: center;
  background-color: ${({ theme }) => theme.colors.button};
  color: white;
  text-transform: uppercase;
  cursor: pointer;
`;
