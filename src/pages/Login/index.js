/* eslint-disable no-undef */
import { useCallback, useState } from "react";

import Input from "../../components/Input";
import {
  LoginContainer,
  LoginContent,
  LoginForm,
  LoginButton,
  WelcomeText,
  WelcomeHelperText,
  Logo,
} from "./styles";
import { theme } from "../../styles/theme";

import { useForm } from "react-hook-form";
import PasswordInput from "../../components/PasswordInput";
import { signIn } from "../../services/Auth";
import { useHistory } from "react-router";

import { EMAIL_REGEX } from "../../constants/regexes";
import LoadingBox from "../../components/LoadingBox";

import { ReactComponent as PasswordIcon } from "../../assets/ic-cadeado.svg";
import { ReactComponent as EmailIcon } from "../../assets/ic-email.svg";
import { ErrorMessage } from "../../components/Input/styles";

const MILLISECONDS_TO_DISMISS_ERROR = 2000;

const Login = () => {
  const { handleSubmit, errors, register } = useForm({
    reValidateMode: "onBlur",
  });

  const [isLoading, setIsLoading] = useState(false);
  const [isCredentialsInvalid, setIsCredentialsInvalid] = useState(false);

  const history = useHistory();

  const login = useCallback(
    async ({ email, password }) => {
      setIsLoading(true);
      try {
        await signIn(email, password);
        history.push("/enterprises");
      } catch {
        setIsCredentialsInvalid(true);
        setTimeout(
          () => setIsCredentialsInvalid(false),
          MILLISECONDS_TO_DISMISS_ERROR
        );
      }
      setIsLoading(false);
    },
    [history]
  );

  return (
    <LoadingBox isLoading={isLoading}>
      <LoginContainer>
        <LoginContent>
          <Logo src={`${process.env.PUBLIC_URL}/logo.png`} alt="ioasys logo" />
          <WelcomeText>Bem-vindo ao empresas</WelcomeText>
          <WelcomeHelperText>
            Faça login abaixo para acessar nossa plataforma.
          </WelcomeHelperText>
          <LoginForm onSubmit={handleSubmit(login)}>
            <Input
              name="email"
              innerRef={register({
                required: "Esse campo é obrigatório, tente novamente.",
                pattern: {
                  value: EMAIL_REGEX,
                  message: "Formato de e-mail inválido, tente novamente.",
                },
              })}
              type="email"
              isInvalid={Boolean(errors.email) || isCredentialsInvalid}
              errorMessage={errors?.email?.message}
              addonLeft={<EmailIcon />}
              alt="E-mail"
              placeholder="E-mail"
              placeholderColor={theme.colors.primary}
            />
            <PasswordInput
              name="password"
              innerRef={register({
                required: "Esse campo é obrigatório, tente novamente.",
              })}
              isInvalid={Boolean(errors.password) || isCredentialsInvalid}
              errorMessage={errors?.password?.message}
              addonLeft={<PasswordIcon />}
              alt="Senha"
              placeholder="Senha"
              placeholderColor={theme.colors.primary}
            />
            {isCredentialsInvalid && (
              <ErrorMessage>
                Credenciais informadas são inválidas, tente novamente.
              </ErrorMessage>
            )}
            <LoginButton type="submit">Entrar</LoginButton>
          </LoginForm>
        </LoginContent>
      </LoginContainer>
    </LoadingBox>
  );
};

export default Login;
