import { useCallback, useEffect, useState } from "react";
import { VscArrowLeft } from "react-icons/vsc";
import { useHistory, useParams } from "react-router";
import Navbar from "../../components/Navbar";
import { getEnterprise } from "../../services/Enterprises";
import {
  EnterpriseDetailsContent,
  EnterpriseImage,
  EnterpriseDescription,
  EnterpriseDetailsContainer,
  NavbarContent,
  EnterpriseName,
} from "./styles";
import LoadingBox from "../../components/LoadingBox";

import { BASE_URL } from "../../constants/url";

const EnterpriseDetails = () => {
  const [enterprise, setEnterprise] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const { id } = useParams();

  const history = useHistory();

  const loadEnterprise = useCallback(async () => {
    setIsLoading(true);
    const response = await getEnterprise(id);
    setEnterprise(response.enterprise);
    setIsLoading(false);
  }, [id]);

  const handleBackClick = useCallback(() => {
    history.push("/enterprises");
  }, [history]);

  useEffect(loadEnterprise, [loadEnterprise]);

  return (
    <LoadingBox isLoading={isLoading}>
      <EnterpriseDetailsContainer>
        <Navbar>
          <NavbarContent>
            <VscArrowLeft onClick={handleBackClick} cursor="pointer" />
            <EnterpriseName>{enterprise?.enterprise_name}</EnterpriseName>
          </NavbarContent>
        </Navbar>
        <EnterpriseDetailsContent>
          {enterprise?.photo && (
            <EnterpriseImage src={`${BASE_URL}/${enterprise.photo}`} />
          )}
          <EnterpriseDescription>
            {enterprise?.description}
          </EnterpriseDescription>
        </EnterpriseDetailsContent>
      </EnterpriseDetailsContainer>
    </LoadingBox>
  );
};

export default EnterpriseDetails;
