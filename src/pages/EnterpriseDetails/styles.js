import styled from "styled-components";

export const EnterpriseDetailsContainer = styled.div`
  background-color: ${({ theme }) => theme.colors.background};
  min-height: 100vh;
`;

export const EnterpriseDetailsContent = styled.div`
  background-color: white;
  padding: 3rem 7rem;
  border-radius: 4.7px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 80%;
  margin: 0 auto;
  margin-top: 3rem;

  @media (max-width: 768px) {
    padding: 1rem 2rem;
  }
`;

export const EnterpriseImage = styled.img`
  object-fit: contain;
  max-width: 48.5rem;
  margin-bottom: 3rem;
  @media (max-width: 768px) {
    max-width: 17rem;
    margin-bottom: 1.5rem;
  }
`;

export const EnterpriseDescription = styled.div`
  font-size: 2.125rem;
  color: ${({ theme }) => theme.colors.enterpriseText};
  @media (max-width: 768px) {
    font-size: 1.25rem;
  }
`;

export const NavbarContent = styled.div`
  font-size: 2.125rem;
  text-transform: uppercase;

  @media (max-width: 768px) {
    font-size: 1.25rem;
  }
`;

export const EnterpriseName = styled.span`
  margin-left: 5rem;

  @media (max-width: 768px) {
    margin-left: 2rem;
  }
`;
