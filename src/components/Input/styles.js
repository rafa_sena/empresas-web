import styled from "styled-components";

export const Input = styled.input`
  margin-left: 0.2rem;
  background-color: transparent;
  font-family: Roboto;
  font-size: 1.369rem;
  letter-spacing: -0.31px;
  border: none;
  flex: 1;
  color: ${({ fontColor }) => fontColor};
  caret-color: ${({ caretColor }) => caretColor};

  &::placeholder {
    color: ${({ theme, placeholderColor }) =>
      placeholderColor || theme.colors.searchPlaceholder};
    font-size: 1.125rem;
    letter-spacing: -0.25px;
    text-align: left;
  }
`;

export const InputContainer = styled.div`
  display: flex;
  width: 100%;
  border-bottom: ${({ theme, borderColor }) =>
    borderColor
      ? `solid 0.6px ${borderColor}`
      : `solid 0.6px ${theme.colors.primary}`};

  &.invalid-input {
    border-color: ${({ theme }) => theme.colors.error};
  }
`;

export const InputIcon = styled.img`
  position: relative;
  bottom: -2px;
`;

export const ErrorMessage = styled.span`
  font-size: 0.76rem;
  line-height: 1.95;
  letter-spacing: -0.17px;
  color: ${({ theme }) => theme.colors.error};
  text-align: center;
`;
