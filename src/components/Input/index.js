import { InputContainer, Input, ErrorMessage } from "./styles";
import { AiFillExclamationCircle } from "react-icons/ai";

import { theme } from "../../styles/theme";
import { memo } from "react";

const InputComponent = memo(
  ({
    innerRef,
    isInvalid,
    errorMessage,
    addonLeft,
    addonRight,
    borderColor,
    ...rest
  }) => {
    return (
      <>
        <InputContainer
          borderColor={borderColor}
          {...(isInvalid && { className: "invalid-input" })}
        >
          {addonLeft && <span>{addonLeft}</span>}
          <Input ref={innerRef} {...rest} />
          {addonRight && <span>{addonRight}</span>}
          {isInvalid && (
            <AiFillExclamationCircle color={theme.colors.error} size="1.5rem" />
          )}
        </InputContainer>
        {isInvalid && errorMessage && (
          <ErrorMessage>{errorMessage}</ErrorMessage>
        )}
      </>
    );
  }
);

export default InputComponent;
