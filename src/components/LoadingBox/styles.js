import styled from "styled-components";

export const LoadingOverlay = styled.div`
  display: ${({ shouldDisplay }) => (shouldDisplay ? "flex" : "none")};
  height: 100vh;
  width: 100vw;
  align-items: center;
  justify-content: center;
  background-color: rgba(255, 255, 255, 0.6);
  position: absolute;
`;
