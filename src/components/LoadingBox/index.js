import { memo } from "react";
import Lottie from "react-lottie";
import loadingAnimation from "../../assets/lottie/spinner.json";
import { LoadingOverlay } from "./styles";

const LoadingBox = memo(({ isLoading, children }) => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: loadingAnimation,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <div>
      <LoadingOverlay shouldDisplay={isLoading}>
        <Lottie options={defaultOptions} width="10rem" height="10rem" />
      </LoadingOverlay>
      {children}
    </div>
  );
});

export default LoadingBox;
