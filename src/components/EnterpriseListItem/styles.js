import styled from "styled-components";

export const EnterpriseContainer = styled.div`
  background-color: white;
  padding: 1.688rem 15.86rem 1.679rem 1.914rem;
  border-radius: 4.7px;
  display: flex;
  flex-direction: row;
  width: 95%;
  cursor: pointer;
  margin: 1rem 0;
`;

export const EnterpriseImage = styled.img`
  object-fit: contain;
  max-width: 100%;
`;

export const EnterpriseContent = styled.div`
  text-align: left;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: 2.397rem;

  & h1 {
    font-size: 1.875rem;
    color: ${({ theme }) => theme.colors.enterpriseTitle};
    @media (max-width: 768px) {
      font-size: 1.5rem;
    }
  }
  & h2 {
    font-size: 1.5rem;
    color: ${({ theme }) => theme.colors.enterpriseText};
    @media (max-width: 768px) {
      font-size: 1.25rem;
    }
  }
  & p {
    font-size: 1.125rem;
    color: ${({ theme }) => theme.colors.enterpriseText};
    @media (max-width: 768px) {
      font-size: 1rem;
    }
  }
`;
