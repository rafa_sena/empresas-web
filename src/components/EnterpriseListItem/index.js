import { memo } from "react";
import {
  EnterpriseContainer,
  EnterpriseImage,
  EnterpriseContent,
} from "./styles";

const EnterpriseListItem = memo(
  ({ imageUrl, name, business, location, ...rest }) => {
    return (
      <EnterpriseContainer {...rest}>
        {imageUrl && <EnterpriseImage src={imageUrl} />}
        <EnterpriseContent>
          <h1>{name}</h1>
          <h2>{business}</h2>
          <p>{location}</p>
        </EnterpriseContent>
      </EnterpriseContainer>
    );
  }
);

export default EnterpriseListItem;
