import styled from "styled-components";

export const NavbarContainer = styled.div`
  height: 8rem;
  padding: 0 3rem;
  display: flex;
  color: white;
  justify-content: space-between;
  align-items: center;
  background-image: ${({ theme }) =>
    `linear-gradient(177deg, ${theme.colors.secondary} 66%, ${theme.colors.navbarBlue} 190%)`};
`;
