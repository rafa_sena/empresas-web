import { memo } from "react";
import { NavbarContainer } from "./styles";

const Navbar = memo(({ children }) => {
  return <NavbarContainer>{children}</NavbarContainer>;
});

export default Navbar;
