import { useState } from "react";
import Input from "../Input";

import { AiFillEyeInvisible, AiFillEye } from "react-icons/ai";

const PasswordInput = ({ ...rest }) => {
  const [isShowingPassword, setIsShowingPassword] = useState(false);

  const handlePasswordShowClick = () =>
    setIsShowingPassword(!isShowingPassword);

  return (
    <Input
      {...rest}
      type={isShowingPassword ? "text" : "password"}
      addonRight={
        isShowingPassword ? (
          <AiFillEye
            onClick={handlePasswordShowClick}
            size="1.5em"
            color="rgba(0, 0, 0, 0.54)"
          />
        ) : (
          <AiFillEyeInvisible
            onClick={handlePasswordShowClick}
            size="1.5em"
            color="rgba(0, 0, 0, 0.54)"
          />
        )
      }
    />
  );
};

export default PasswordInput;
